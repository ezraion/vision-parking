# We will be sending signals out from our Raspberry Pi

# We will need the time module
import time

# First, import the GPIO module
import RPi.GPIO as GPIO

# Set the numbering mode for the pins
GPIO.setmode(GPIO.BOARD)

# Set pin 12 in output mode
GPIO.setup(5, GPIO.OUT)

# Now, turn the LED on, sleep for 1 second, then turn it off
GPIO.output(5, GPIO.HIGH)
time.sleep(0.5)
GPIO.output(5, GPIO.LOW)

# Now that we are done, we can cleanup
GPIO.cleanup()
