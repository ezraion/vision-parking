from pirc522 import RFID

rdr1 = RFID()
(error, data) = rdr1.request()
(error, uid) = rdr1.anticoll()
rdr1.cleanup()

rdr = RFID()

(error, data) = rdr.request()
if not error:
    (error, uid) = rdr.anticoll()
    if not error:
        hexuid = format(uid[3], "02X") + format(uid[2], "02X") + format(uid[1], "02X") + format(uid[0], "02X")
        print(str(int(hexuid,16)))

rdr.cleanup()
