/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.GsonBuilder;

/**
 *
 * @author Alan
 */
public class BaseObject {
    public BaseObject(){
    }
    
    public String toJson(){
        String json = new GsonBuilder().create().toJson(this);

        return json;
    }
}
