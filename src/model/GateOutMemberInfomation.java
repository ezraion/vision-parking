/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Image;
import javax.swing.Icon;

/**
 *
 * @author Alan
 */
public class GateOutMemberInfomation {
    private Icon inImage;
    private Icon outImage;
    private String plat;
    private String cardId;
    private String inTime;
    private String outTime;
    private String duration;
    private String price;
    private String balance;
    private String vehicleBrand;
    private String vehicleType;
    private String vehicleColor;
    private String parkingID;

    public GateOutMemberInfomation(Icon inImage, Icon outImage, String plat, String cardId, String inTime, String outTime, String duration, String price, String balance, String vehicleBrand, 
            String vehicleType, String vehicleColor, String parkingID) {
        this.inImage = inImage;
        this.outImage = outImage;
        this.plat = plat;
        this.cardId = cardId;
        this.inTime = inTime;
        this.outTime = outTime;
        this.duration = duration;
        this.price = price;
        this.balance = balance;
        this.vehicleBrand = vehicleBrand;
        this.vehicleType = vehicleType;
        this.vehicleColor = vehicleColor;
        this.parkingID = parkingID;
    }

    public Icon getInImage() {
        return inImage;
    }

    public void setInImage(Icon inImage) {
        this.inImage = inImage;
    }

    public Icon getOutImage() {
        return outImage;
    }

    public void setOutImage(Icon outImage) {
        this.outImage = outImage;
    }
    
    public String getPlat() {
        return plat;
    }

    public void setPlat(String plat) {
        this.plat = plat;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getVehicleBrand() {
        return vehicleBrand;
    }

    public void setVehicleBrand(String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public String getParkingID() {
        return parkingID;
    }

    public void setParkingID(String parkingID) {
        this.parkingID = parkingID;
    }
    
    
    
}
