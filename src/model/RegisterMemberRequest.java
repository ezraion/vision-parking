/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class RegisterMemberRequest {
    private String username;
    private String email;
    private String phoneNumber, alamatTinggal, alamatKTP, noKTP, password, photo;
    
    public RegisterMemberRequest(String username, String email, String phoneNumber, String alamatTinggal, String alamatKTP,
            String noKTP, String password, String photo) {
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.alamatTinggal = alamatTinggal;
        this.alamatKTP = alamatKTP;
        this.noKTP = noKTP;
        this.password = password;
        this.photo = photo;
    }
}
