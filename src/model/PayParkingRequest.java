/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class PayParkingRequest {
    int payment_type_id;
    String parkingID;

    public int getPaymentTypeID() {
        return payment_type_id;
    }

    public void setPaymentTypeID(int paymentTypeID) {
        this.payment_type_id = paymentTypeID;
    }

    public String getParkingID() {
        return parkingID;
    }

    public void setParkingID(String parkingID) {
        this.parkingID = parkingID;
    }
}
