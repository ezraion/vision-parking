/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Alan
 */
public class GateInRequest {
    private String gateID;
    private String photo;
    private String vehicleTypeID;
    private String cardID;
    private boolean isMember;
    private String photo2;

    public GateInRequest(String gateID, String photo, String vehicleTypeID, String cardID, boolean isMember, String photo2) {
        this.gateID = gateID;
        this.photo = photo;
        this.vehicleTypeID = vehicleTypeID;
        this.cardID = cardID;
        this.isMember = isMember;
        this.photo2 = photo2;
        //System.out.println("model.GateInRequest.<init>() VEHICLE TYPE ID: "+vehicleTypeID);
    }
}
