/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class NonMemberGateOutRequest {
    private String gateID;
    private String photo;
    private String photo2;
    private String cardID;
    private String plateNumber;
    private boolean isMember = true;
    
    public NonMemberGateOutRequest(String gateID, String photo, String cardID, String plateNumber, String photo2)
    {
        this.gateID = gateID;
        this.photo = photo;
        this.photo2 = photo2;
        this.cardID = cardID;
        this.plateNumber = plateNumber;
    }

    public String getGateID() {
        return gateID;
    }

    public void setGateID(String gateID) {
        this.gateID = gateID;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }
}
