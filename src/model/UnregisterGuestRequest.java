/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class UnregisterGuestRequest {
    private String cardID;
    private String reason;

    public UnregisterGuestRequest(String cardID, String reason) {
        if(reason.equals(""))
            this.reason = "-";
        else
            this.reason = reason;
        this.cardID = cardID;
    }
}
