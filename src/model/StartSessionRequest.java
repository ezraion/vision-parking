/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class StartSessionRequest {
    private String managerPassword;
    private String gateID;
    private int startingBalance;
    
    public StartSessionRequest(String managerPassword, String gateID, int startingBalance)
    {
        this.managerPassword = managerPassword;
        this.gateID = gateID;
        this.startingBalance = startingBalance;
    }
}
