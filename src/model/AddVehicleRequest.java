/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class AddVehicleRequest {
    private int vehicleTypeID;
    private String memberID;
    private String cardID;
    private String name;
    private String plateNumber;
    private String address;
    private String noSTNK;
    private String brand;
    private String color;
    private String type;
    private String photo;
    
    public AddVehicleRequest(int vehicleTypeID, String memberID, String cardID, String name,
            String plateNumber, String address, String noSTNK, String brand, String color,
            String type, String photo) {
        this.vehicleTypeID = vehicleTypeID;
        this.memberID = memberID;
        this.cardID = cardID;
        this.name = name;
        this.plateNumber = plateNumber;
        this.address = address;
        this.noSTNK = noSTNK;
        this.brand = brand;
        this.color = color;
        this.type = type;
        this.photo = photo;
    }
}
