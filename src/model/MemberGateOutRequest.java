/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class MemberGateOutRequest {
    private String gateID;
    
    public MemberGateOutRequest(String gateID, String photo, String cardID, String photo2)
    {
        this.gateID = gateID;
        this.photo = photo;
        this.cardID = cardID;
        this.photo2 = photo2;
    }

    public String getGateID() {
        return gateID;
    }

    public void setGateID(String gateID) {
        this.gateID = gateID;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }
    private String photo;
    private String cardID;
    private boolean isMember = true;
    private String photo2;
}
