/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Alan
 */
public class ReportProblemRequest extends BaseObject{
    private boolean isParkingRelated;
    private String problemID;
    private String reason;
    private String cardID;
    private String parkingID;
    private int parkingLotID;
    private String description;

    public ReportProblemRequest(boolean isParkingRelated, String problemID, String reason, String cardID, String parkignID, int parkingLotID) {
        this.isParkingRelated = isParkingRelated;
        this.problemID = problemID;
        this.reason = reason;
        this.cardID = cardID;
        this.parkingID = parkignID;
        this.parkingLotID = parkingLotID;
    }
    
    public ReportProblemRequest(boolean isParkingRelated, String problemID, String reason,  int parkingLotID, String description) {
        this.isParkingRelated = isParkingRelated;
        this.problemID = problemID;
        this.reason = reason;
        this.description = description;
        this.parkingLotID = parkingLotID;
    }
}
