/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class RegisterGuestRequest {
    private String cardID;
    private String name;
    private String plateNumber;
    private String phoneNumber;
    private String photo;

    public RegisterGuestRequest(String photo, String cardID, String name, String plateNumber, String phoneNumber) {
        this.name = name;
        this.photo = photo;
        this.plateNumber = plateNumber;
        if(phoneNumber.equals(""))
            this.phoneNumber = "-";
        else
            this.phoneNumber = phoneNumber;
        this.cardID = cardID;
    }
}
