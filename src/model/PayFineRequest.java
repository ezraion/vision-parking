/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class PayFineRequest {
    private String parkingProblemID;
    private int payment_type_id;
    private int parkingLotID;
    
    public PayFineRequest(String problemID, int paymentTypeID, int parkingLotID)
    {
        this.parkingProblemID = problemID;
        this.payment_type_id = paymentTypeID;
        this.parkingLotID = parkingLotID;
    }

    public String getProblemID() {
        return parkingProblemID;
    }

    public void setProblemID(String ProblemID) {
        this.parkingProblemID = ProblemID;
    }

    public int getPaymentTypeID() {
        return payment_type_id;
    }

    public void setPaymentTypeID(int paymentTypeID) {
        this.payment_type_id = paymentTypeID;
    }

    public int getParkingLotID() {
        return parkingLotID;
    }

    public void setParkingLotID(int parkingLotID) {
        this.parkingLotID = parkingLotID;
    }
}
