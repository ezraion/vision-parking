/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ferico
 */
public class TopUpRequest {
    private int topUpAmountID;
    private String cardID;
    
    public TopUpRequest(int topUpAmountID, String cardID) {
        this.topUpAmountID = topUpAmountID;
        this.cardID = cardID;
    }

    public int getTopUpAmountID() {
        return topUpAmountID;
    }

    public void setTopUpAmountID(int topUpAmountID) {
        this.topUpAmountID = topUpAmountID;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }
}
