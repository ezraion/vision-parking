/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Alan
 */
public class Problem {
    private String id;
    private String problem;
    private String description;
    private int fine;
    private int isParkingRelated;
    private String auditedUser;
    private String auditedActivity;
    private String created_at;
    private String deleted_at;

    public Problem(String id, String problem, String description, int fine, int isParkingRelated, String auditedUser, String auditedActivity, String created_at, String deleted_at) {
        this.id = id;
        this.problem = problem;
        this.description = description;
        this.fine = fine;
        this.isParkingRelated = isParkingRelated;
        this.auditedUser = auditedUser;
        this.auditedActivity = auditedActivity;
        this.created_at = created_at;
        this.deleted_at = deleted_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFine() {
        return fine;
    }

    public void setFine(int fine) {
        this.fine = fine;
    }

    public int getIsParkingRelated() {
        return isParkingRelated;
    }

    public void setIsParkingRelated(int isParkingRelated) {
        this.isParkingRelated = isParkingRelated;
    }

    public String getAuditedUser() {
        return auditedUser;
    }

    public void setAuditedUser(String auditedUser) {
        this.auditedUser = auditedUser;
    }

    public String getAuditedActivity() {
        return auditedActivity;
    }

    public void setAuditedActivity(String auditedActivity) {
        this.auditedActivity = auditedActivity;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }
    
    
}
