/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Alan
 */
public class RequestData extends BaseObject implements Serializable{
    private Object data;
    private String apiKey;

    public RequestData() {
    }

    public RequestData(Object data, String apiKey) {
        this.data = data;
        this.apiKey = apiKey;
    }
    
}
