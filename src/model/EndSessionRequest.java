/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Alan
 */
public class EndSessionRequest {
    private String managerPassword;
    private int finalBalance;
    private int takenBalance;

    public EndSessionRequest() {
    }

    public EndSessionRequest(String managerPassword, int finalBalance, int takenBalance) {
        this.managerPassword = managerPassword;
        this.finalBalance = finalBalance;
        this.takenBalance = takenBalance;
    }
    
    
}
