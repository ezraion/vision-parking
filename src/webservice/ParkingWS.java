/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import model.AddVehicleRequest;
import model.EndSessionRequest;
import model.GateInRequest;
import model.ImportRequest;
import model.LoginInformation;
import model.MemberGateOutRequest;
import model.NonMemberGateOutRequest;
import model.PayFineRequest;
import model.PayParkingRequest;
import model.RegisterGuestRequest;
import model.RegisterMemberRequest;
import model.ReportProblemRequest;
import model.RequestData;
import model.StartSessionRequest;
import model.TopUpRequest;
import model.UnregisterGuestRequest;
import util.Util;

/**
 *
 * @author Ferico
 */
public class ParkingWS {
    
    public void login(LoginInformation loginInformation, WSListener callback)
    {
        RequestData req = new RequestData(loginInformation, Util.API_KEY);
        new PostRequest(false, Util.LOGIN_URL(), req.toJson(), callback);
    }
    
    public void getParkingLotManager(WSListener callback)
    {
        new GetRequest(true, Util.GET_PARKING_LOT_MANAGER_URL().replace("{parkingLotID}", 
                Util.PARKING_LOT_ID+""), callback);
    }
    
    public void startSession(int managerID, int startingBalance, String managerPassword, WSListener callback)
    {
        StartSessionRequest data = new StartSessionRequest(managerPassword, Util.GATE_ID, startingBalance);
        RequestData requestData = new RequestData(data, Util.API_KEY);
        String url = Util.START_SESSION_URL().replace("{parkingLotID}", Util.PARKING_LOT_ID+"");
        url = url.replace("{managerID}", managerID+"");
        new PostRequest(true, url, requestData.toJson(), callback);
    }
    
    public void getTopUpAmounts(WSListener callback)
    {
        new GetRequest(false, Util.TOPUP_AMOUNT_URL(), callback);
    }
    
    public void getVehicleDetailByCardID(String cardID, WSListener callback)
    {
        cardID = cardID.replace(" ", "%20");
        String url = Util.GET_VEHICLE_DETAIL_BY_CARD_URL().replace("{cardID}", cardID);
        new GetRequest(true, url, callback);
    }
    
    public void EndSession(int managerID, int finalBalance, int takenBalance, String managerPassword, WSListener callback)
    {
        System.out.println(managerPassword);
        EndSessionRequest data = new EndSessionRequest(managerPassword, finalBalance, takenBalance);
        RequestData requestData = new RequestData(data, Util.API_KEY);
        System.out.println(Util.SESSION_ID);
        String url = Util.END_SESSION_URL().replace("{sessionID}", Util.SESSION_ID+"");
        url = url.replace("{managerID}", managerID+"");
        
        new PostRequest(true, url, requestData.toJson(), callback);
    }
    
    public void topUp(int topUpAmountID, String cardID, WSListener callback)
    {
        TopUpRequest data = new TopUpRequest(topUpAmountID, cardID);
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(true, Util.TOP_UP_URL(), requestData.toJson(), callback);
    }
    
    public void gateOut(String photo, String parkingID, String plateNumber, WSListener callback, String photo2)
    {
        NonMemberGateOutRequest data = new NonMemberGateOutRequest(Util.GATE_ID, photo, parkingID, plateNumber, photo2);
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(false, Util.GATE_OUT_URL(), requestData.toJson(), callback);
    }
    
    public void getPaymentType(WSListener callback)
    {
        new GetRequest(false, Util.GET_PAYMENT_TYPE_URL(), callback);
    }
    
    public void payParking(String parkingID, int paymentTypeID, WSListener callback)
    {
        PayParkingRequest data = new PayParkingRequest();
        data.setParkingID(parkingID);
        data.setPaymentTypeID(paymentTypeID);
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(false, Util.PAY_PARKING_URL(), requestData.toJson(), callback);
    }
    
    public void gateOut(String photo, String cardID, WSListener callback, String photo2)
    {
        MemberGateOutRequest data = new MemberGateOutRequest(Util.GATE_ID, photo, cardID, photo2);
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(false, Util.GATE_OUT_URL(), requestData.toJson(), callback);
    }
    
    public void getProblemDetail(String problemID, WSListener callback)
    {
        problemID = problemID.replace(" ","%20");
        String url = Util.GET_PROBLEM_DETAIL_URL();
        url = url.replace("{parkingLotID}",Util.PARKING_LOT_ID+"");
        url = url.replace("{problemID}",problemID);
        new GetRequest(false, url, callback);
    }
    
    public void payFine(String problemID, int paymentTypeID, WSListener callback)
    {
        //problemID = problemID.replace(" ","%20");
        PayFineRequest data = new PayFineRequest(problemID, paymentTypeID, Util.PARKING_LOT_ID);
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(true, Util.PAY_FINE_URL(), requestData.toJson(), callback);
    }
    
    public void getProblems(WSListener callback){
        new GetRequest(false, Util.GET_PROBLEMS(), callback);
    }
    
    public void reportProblem(ReportProblemRequest data, WSListener callback){
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(true, Util.REPORT_PROBLEMS(), requestData.toJson(), callback);
    }

    public void gateIn(GateInRequest data, WSListener callback){
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(false, Util.GATE_IN_URL(), requestData.toJson(), callback);
    }
    
    public void registerGuest(RegisterGuestRequest data, WSListener callback) {
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(true, Util.REGISTER_GUEST_URL(), requestData.toJson(), callback);
    }
    
    public void registerMember(RegisterMemberRequest data, WSListener callback) {
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(false, Util.REGISTER_MEMBER_URL(), requestData.toJson(), callback);
    }
    
    public void getGuestDetail(String cardID, WSListener callback) {
        cardID = cardID.replace(" ","%20");
        String url = Util.GET_GUEST_DETAIL_URL();
        url = url.replace("{cardID}",cardID);
        new GetRequest(true, url, callback);
    }
    
    public void unregisterGuest(UnregisterGuestRequest data, WSListener callback) {
        RequestData requestData = new RequestData(data, Util.API_KEY);
        new PostRequest(true, Util.UNREGISTER_GUEST_URL(), requestData.toJson(), callback);
    }
    
    public void getVehicleTypes(WSListener callback) {
        new GetRequest(true, Util.GET_VEHICLE_TYPE_URL(), callback);
    }
    
    public void getMemberID(String username, WSListener callback) {
        String url = Util.GET_MEMBER_ID_URL();
        url = url.replace("{username}",username);
        new GetRequest(true, url, callback);
    }
    
    public void addVehicle(AddVehicleRequest request, WSListener callback) {
        RequestData requestData = new RequestData(request, Util.API_KEY);
        new PostRequest(true, Util.ADD_VEHICLE_URL(), requestData.toJson(), callback);
    }
    
    public void importMember(ImportRequest request, WSListener callback) {
        RequestData requestData = new RequestData(request, Util.API_KEY);
        new PostRequest(true, Util.IMPORT_MEMBER_DATA_URL(), requestData.toJson(), callback);
    }
    
    public void exportMember(WSListener callback) {
        new GetRequest(true, Util.EXPORT_MEMBER_DATA_URL(), callback);
    }
}
