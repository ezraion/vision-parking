/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.JsonObject;

/**
 *
 * @author Ferico
 */
public interface WSListener {
    
    void onSuccess(JsonObject jsonObject);
    void onError(String error, JsonObject jsonObject);
    void shouldLogOut();
    
}
