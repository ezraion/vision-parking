/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.sun.imageio.plugins.common.I18N;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.http.client.fluent.Async;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.StringEntity;
import util.HeaderBuilder;
import visionparking.LoginFrame;

/**
 *
 * @author Ferico
 */
public class PostRequest extends BaseRequest {

    public PostRequest(boolean isUsingHeader, String url, String body, final WSListener callback) {
        this.callback = callback;
        System.out.println(url);
        ExecutorService threadpool = Executors.newFixedThreadPool(1);
        Async async = Async.newInstance().use(threadpool);
        
        try {
            Request request = Request.Post(url)
                    .body(new StringEntity(body))
                    .setHeader("Content-type", "application/json");
            if (isUsingHeader) {
                System.out.println(HeaderBuilder.getBearer());
                request.setHeader("Authorization", "Bearer "+HeaderBuilder.getBearer());
            }
       
            
            Future<String> future = async.execute(request, rh, new FutureCallback<String>() {
                @Override
                public void completed(String response) {
                   if(response.equals("401"))
                    {
                        callback.shouldLogOut();
                        return;
                    }

                   try {
                    JsonElement element = new JsonParser().parse(response);
                    JsonObject jsonObject = element.getAsJsonObject();
                    if (callback == null) {
                        return;
                    }
                    if (jsonObject.get("success").toString().equals("true")) {
                        callback.onSuccess(jsonObject);
                    } else {
                        String error = jsonObject.get("error").toString();
                        callback.onError(error, jsonObject);
                    }
                   }
                   catch(JsonSyntaxException ex) {
                       callback.onError("Something went wrong! please try again later", null);
                   }
                }

                @Override
                public void failed(Exception excptn) {
                    JOptionPane.showMessageDialog(null, excptn.getMessage(), "error", JOptionPane.ERROR_MESSAGE);
                    //callback.onError(excptn.getMessage());
                    Logger.getLogger(LoginFrame.class.getName()).log(Level.SEVERE, null, excptn);
                }

                @Override
                public void cancelled() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });
            
            
        } catch (IOException ex) {
            Logger.getLogger(LoginFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean isUnique(String str)
    {
        int checker = 0;
        
        for(int i=0; i<str.length(); i++) 
        {
            int val = str.charAt(i) - 'a';
            if((checker & (1 << val)) > 0)
            {
                return false;
            }
            checker |= (1<<val);
        }
        return true;
    }
}
