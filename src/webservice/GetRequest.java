/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.http.client.fluent.Async;
import org.apache.http.client.fluent.Request;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.StringEntity;
import util.HeaderBuilder;
import visionparking.LoginFrame;

/**
 *
 * @author Ferico
 */
public class GetRequest extends BaseRequest {
    public GetRequest(boolean isUsingHeader, String url, final WSListener callback)
    {
        this.callback = callback;
        System.out.println(url);
        ExecutorService threadpool = Executors.newFixedThreadPool(1);
        Async async = Async.newInstance().use(threadpool);
        Request request = Request.Get(url)
                .setHeader("Content-type", "application/json");
        if (isUsingHeader) {
            request.setHeader("Authorization", "Bearer "+HeaderBuilder.getBearer());
            System.out.println(HeaderBuilder.getBearer());
        }
        Future<String> future = async.execute(request, rh, new FutureCallback<String>() {
            @Override
            public void completed(String response) {
                if(response.equals("401"))
                {
                    callback.shouldLogOut();
                    return;
                }
                
                try {
                    JsonElement element = new JsonParser().parse(response);
                    JsonObject jsonObject = element.getAsJsonObject();
                    if (callback == null) {
                        System.out.println(".completed() null");
                        return;
                    }
                    if (jsonObject.get("success").toString().equals("true")) {
                        callback.onSuccess(jsonObject);
                        System.out.println(".completed()");
                    } else {
                        //System.out.println(".completed() error");
                        String error = jsonObject.get("error").toString();
                        callback.onError(error, jsonObject);
                        System.out.println(".completed() error");
                    }
                }
                catch(JsonSyntaxException exception) {
                    callback.onError("Something went wrong!", null);
                }
            }
            
            @Override
            public void failed(Exception excptn) {
                JOptionPane.showMessageDialog(null, excptn.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(LoginFrame.class.getName()).log(Level.SEVERE, null, excptn);
            }
            
            @Override
            public void cancelled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
}
