/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;

/**
 *
 * @author Ferico
 */
public class BaseRequest {
    protected WSListener callback;
    protected ResponseHandler<String> rh = new ResponseHandler<String>(){
        @Override
        public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
            StatusLine statusLine = response.getStatusLine();
            HttpEntity entity = response.getEntity();
            if (statusLine.getStatusCode() == 401) {
                System.out.println("401");
                return "401";
            }
            //ini ga perlu di cek begini lagi -> bingung juga handle nya, liat dari result nya aja langsung nanti
//                throw new HttpResponseException(
//                    statusLine.getStatusCode(),
//                    statusLine.getReasonPhrase());
//            }
//            if (entity == null) {
//              throw new ClientProtocolException("Response contains no content");
//            }
            ContentType contentType = ContentType.getOrDefault(entity);
            Charset charset = contentType.getCharset();
            
            String result = IOUtils.toString(entity.getContent(), "UTF-8");
            
            System.out.println(result);
            return result;
        }
    };
}
