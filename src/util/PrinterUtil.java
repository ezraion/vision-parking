/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.event.PrintJobAdapter;
import javax.swing.JFrame;
import ui.TicketResultFrame;


/**
 *
 * @author Alan
 */
public class PrinterUtil {
    public static void print(String filePath) throws FileNotFoundException, PrintException, IOException{
    
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        
        pras.add(new Copies(1));
        PrintService pss[] = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.GIF, pras);
        if (pss.length == 0)
          throw new RuntimeException("No printer services available.");
        PrintService ps = pss[0];
        
        
        System.out.println("Printing to " + ps);
        DocPrintJob job = ps.createPrintJob();
        FileInputStream fin = new FileInputStream(filePath);
        Doc doc = new SimpleDoc(fin, DocFlavor.INPUT_STREAM.GIF, null);
        
        job.print(doc, pras);
        fin.close();
    }
    
    public static void printFrame(JFrame frame) throws FileNotFoundException, PrintException, IOException, PrinterException{
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        pras.add(new Copies(1));
        pras.add(Chromaticity.MONOCHROME);
        
        PrintService pss[] = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.GIF, pras);
        if (pss.length == 0)
          throw new RuntimeException("No printer services available.");
        PrintService ps = pss[0];
        
        
        System.out.println("Printing to " + ps);
        
//        DocPrintJob job = ps.createPrintJob();
// 
//        Doc doc = new SimpleDoc(frame, DocFlavor.INPUT_STREAM.GIF, null);
//        
//        job.print(doc, pras);

        
        
        PrinterJob pjob = PrinterJob.getPrinterJob();
        pjob.setPrintable(new PrinterWrapper(frame));
        pjob.setPrintService(ps);
        pjob.print();
        
    }
}
