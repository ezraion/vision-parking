/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 *
 * @author Ferico
 */
public class HeaderBuilder {
    public static String getBearer()
    {
        String combined = Util.ACCESS_TOKEN+":"+Util.USER_ID+":"+Util.API_KEY;
        combined = combined.replace("\"","");
        String base64 = "";
        try {
            byte[] data = combined.getBytes("UTF-8");
            base64 = Base64.getEncoder().encodeToString(data);
            base64 = base64.replace("\n","");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return base64;
    }
}
