/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ferico
 */
public class GateOpener {
    public static void open()
    {
        try {
            if(Util.GATE_DIRECTORY.equals(" "))
                return;
            Runtime.getRuntime().exec("python3 "+Util.GATE_DIRECTORY);
        } catch (IOException ex) {
            Logger.getLogger(GateOpener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
