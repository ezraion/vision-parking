/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Alan
 */
public class QRCodeUtil {
    public static void createQRCode(String qrCodeData, String filePath, int qrCodeheight, int qrCodewidth)
			throws WriterException, IOException {
        
        Map hintMap = new HashMap();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        
        BitMatrix matrix = new MultiFormatWriter().encode(
                        new String(qrCodeData.getBytes("UTF-8"), "UTF-8"),
                        BarcodeFormat.QR_CODE, qrCodewidth, qrCodeheight, hintMap);
        
        MatrixToImageWriter.writeToFile(matrix, filePath.substring(filePath
                        .lastIndexOf('.') + 1), new File(filePath));
    }
}
