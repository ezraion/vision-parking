/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.github.sarxos.webcam.Webcam;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import model.GateOutMemberInfomation;
import org.apache.commons.codec.binary.Base64;
import visionparking.LoginFrame;

/**
 *
 * @author Ferico
 */
public class Util {

    public final static String API_KEY = "263b7f33fdfb189bb5bac3a6ba021637";
    public static String BASE_URL = "http://dev.wintechindo.com/API/";
    public static String READ_DIRECTORY = "/home/pi/vision-parking/pi-rc522/ChipReader/Read.py";
    public static String GATE_DIRECTORY = "/home/pi/Vision-Parking/open.py";
    public static String ACCESS_TOKEN = "";
    public static int USER_ID = 0;
    public static String SESSION_ID = "";
    public static int PARKING_LOT_ID = 0;
    public static String GATE_ID = "";
    public static String GATE_TYPE = "";
    public static String VEHICLE_TYPE_ID = "";
    public static GateOutMemberInfomation GATE_OUT_MEMBER_INFO;

    public static final String GATE_TYPE_IN = "In Gate";
    public static final String GATE_TYPE_OUT = "Out Gate";
    public static final String GATE_TYPE_AUTOMATIC_OUT = "Automatic Out Gate";
    public static final String TEST_PHOTO = "iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAIAAAD2HxkiAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACldJREFUeNrs3Wtv03YbwGEoZRyebRLivA22aYhNQki82vf/Apu2Fdi6lh62MppTW0jTloYkz/20kpenFJo4dg72db1AiIMLrn+9/3Yd53yv1zsHTM55EYIIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhMBsRLi1tbW6umpfM4vu379/8+bNXD/E/Bj+G+12u9ls+nQyi+LozftDzNnLMFkiBBGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERggiBnM1P+b9vbm7uzp07Pk+MolKpdDodEaaP8OHDhw4jRrG1tTXNEVqOgghBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYRARubtgkEcHBy02+0LFy5cvXrV3kCE49Dr9ba3txuNRr1ef/v27f/tsvn5a9eu3bp1K36Mn9tXiDB7r169WltbO9Fe4t27d7UjMRjvHYmf2GmIMButVuv58+fx4yB/uNPpRKv//PPP999/f/36dXuPdFyY+VcsPn/++ecBC0wcHh4uLCz89ddfdiAiHEmlUomWUr+p8srKytLSkt2ICFN68+bN4uLiiBt5+fJlLE3tTEQ4tHa7/fTp0263O/qmlpeXo2e7FBEOJ07n4rwuk01FyS9evCjPrsvkKxdlj/Dg4CCWkRlu8PXr1/V6vSR7L86Em82mikQ4kjiLy/zL+d9//12GXbezsxNfv169eqUiEY6kWq1mvs04LYzzzGLvt06ns7i42Ov1KpVK6kvKiPB/35qP5Wjmm41Ds1arFXvXra6u7u/vH9cYHQpJhOnP33LacrHPlGK/9Z9IW5GKML08xuCx4ylRSHEK/ccff8S07/+K4xszIkzp3bt3OW25wOeEyUK0n2EoQsYkJt7Gxsb7vx6nhfl9RRNhkeX3EqSLFy+WYSHa/1suz4gwjStXruS05UuXLhVyIbq3t/eh33XfrAjT+PTTT3Pa8meffVaShWii1Wrld7VZhIUVqeS0brx27VpJFqKGoQhHcv78+Rs3bmS+2f8cKc9CNFGr1Qp/q5AIs/fVV19Fitlu8/79+6VaiPYPTJdnRJhmat29ezfb88xbt24VaSF6fI/ogH/eilSEaXz99ddZPbkwhup3332X+Wid7EJ0qIfuxKp1Z2fHQSXC4Vy6dOnRo0eZlPPtt98W6ZLM4AvRfu6eEWEaUc6DBw9G3Mjt27eLdDY47EI04fKMCFP68ssvf/jhh7m5lDvk3r178deLtEPW19eHffpjUu/m5qYjSoRp3Llz58mTJ5cvXx7qb8X5ZORXsFPBZrM5ypNUXZ4Z7hCyC/p9/vnnP/7448uXL2MOnHlHcozNu3fvfvPNNwW7U3TAb81/xP7+/vb2dsHuWBDhGNcGc3Oxtoy64jCqVqvx44kaY+LF4XX9+vUbN24U8h7R1AvRE8NQhCIcbb/Mz988cu7oCQ7J/SJR3SeffFLg//iIC9FEo9E4PDws9r4S4fhcuHCheDdk57QQ7d/U5uZmwW4eymvxZRdMp93d3fE/Wjdm4OgL0f4VaSY9i5AJiAXw0yPj7DCyz/a9pQ4ODuKM2mdThDNpfX09juCtra1nz56Np8PjhWjmH8v3KkQ4qwvR5GaxRqMxng5jBsbHzXyz8e//0BseI8LptbS01F9dHMfPnz/PtcPMF6KJOCd0K6kIZ0ys395/SES9Xs+vw5wWoomI0OUZEc6Mdru9urp66m/l12FOC9FELEfj5NYnV4SzYXl5+SOvP8ijw1arldNC9MR498kV4QyIVeiZz4aIDn///fesOoxVYq4L0URMwvzecUCEZHZitri4OMifrNVq0WEmZ1kxA8fzxjXxr/XiJhFOu42NjUEeZ5Z0GOvSETuMhej6+vrY/oPunhHhVNvf319bWxvqr4w4D8e2EE0cHh42Gg2faxFOqeXl5RQ9VKvV1B2ObSHar/8tDRHhFKlUKqlHRHSY4kUPY16IJnZ2dgr8to0inFWdTmdlZWXEhofqMP7k4uLi+F+fcc7dMyKcTi9evBj91sqhOtzY2Jjgu+pubm5OpH8Rcrrd3d2sJkN0OMjjCff29j50R854uDwjwilyfH0yw6v2MWQ+/p3G8V8RPZW7Z0Q4LWIGZn7HZnQYmU3nQjSxvb3t8owIJy/OA+NsMKeTrlPn4cQXooahCNPLY/22srLS6XTym7EnOpyShWj/VwqXZ0Q4xNop8we9NBqNvN/E70SHU7IQTbTb7Vqt5ugS4UBiCbe1tbWwsJBVh7Gd5eXl8Zxz/vnnn+eO7ombnoWoFempPHf0Y2PweIDET6LDx48fp367mMTa2trYLkscH+gTeXTimV6/fh2nqVevXnWYmYRnjMH+IH/77bcRj+Y47FK83d+IHU7VQtQwFGGaMZjY2dn59ddfR7mgMqn7xaZTnBjbGyIcdAz2L6JiHqbrMI6595/gVGbtdrtardoPIhx0DPZ3GPPwzHdNe/+AG8/1mNliRSrC4cZgIvqMeThUh7FB7yB96p7M8K0vRFiKMdh/9Aw+D2N4+pL/IV7pK8Khx2Ci2WxGh2fOt263u7S0ZK9+SJwW5nfzkAgLOwaH6nBjYyPXR+vOulhNuDwjwjRjMBGBfaTDg4ODiTxIYrZ4ub0IU47B/g5/+eWXUzuMhai11plin4//qVMiLMgYTLRarejw8PCw/xfr9bpXkRuGIsx9DPZ3GOvSpMMYgK7HDK5SqZR5ySDCUcfgiXl4/OCmtbU1b445uCgw75d3ibDgYzCxt7cX8zBWoWO+UbsAyvytVBFmMwb7O1xYWPC+C8Pa3d2d2hd8iHBmxiCGoQgnPwYZRbVaHfbOeBEag2Sp2+2W8/JM2SM0Bq1IRWgM8q9Wq1XClz6XOkJj0DAUoTHISbVarWwvgC5vhMbgdOp2u5ubmyI0Bpmkst3PXdIIjcFptre3F18lRWgMMkmlWpGWMUJjcPqV6vJM6SI0BmdCqS7PlC5CY3BWlOcbhuWK0BicIfv7+yW5PFOuCI1Bw1CExiBDqNfrJx6fJUJjkLHq9Xpl+MZ9WSI0BmdURFj4Z4WU5e2y4yz/iy++cEzPordv316+fFmEM0+BWI4CIgQRAiIEEQIiBBECIgQRAiIEEQIiBBGCCAERgggBEYIIgYmY9sdb9Hq9ZrPp88Qout2uCNPrdDo//fSTwwjLUUCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQIZCx871ez14AEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARHC7PqvAAMA/BkrMLAeft8AAAAASUVORK5CYII=";

    public static String LOGIN_URL() {
    return BASE_URL + "user/login";
}
    public static String TOPUP_URL() {
        return BASE_URL + "transaction/topUp";
    }
    public static String TOPUP_AMOUNT_URL() {
        return BASE_URL + "topUpAmount";
    }
    public static String GET_PARKING_LOT_MANAGER_URL() {
        return BASE_URL + "parkingLot/{parkingLotID}/managers";
    }
    public static String START_SESSION_URL() {
        return BASE_URL + "session/{parkingLotID}/start/{managerID}";
    }
    public static String GET_TOP_UP_AMOUNT_URL(){
        return BASE_URL + "topUpAmount";
    }
    public static String GET_VEHICLE_DETAIL_BY_CARD_URL(){
        return BASE_URL + "vehicle/card/{cardID}";
    }
    public static String END_SESSION_URL() {
        return BASE_URL + "session/{sessionID}/end/{managerID}";
    }
    public static String TOP_UP_URL() {
        return BASE_URL + "transaction/topUp";
    }
    public static String GATE_IN_URL() {
        return BASE_URL + "gate/in/gm";
    }
    public static String GATE_OUT_URL() {
        return BASE_URL + "gate/out/gm";
    }
    public static String GET_PAYMENT_TYPE_URL (){
        return BASE_URL + "paymentType";
    }
    public static String PAY_PARKING_URL() {
        return BASE_URL + "parking/pay";
    }
    public static String GET_PROBLEM_DETAIL_URL() {
        return BASE_URL + "parkingLot/{parkingLotID}/fine/{problemID}";
    }
    public static String PAY_FINE_URL() {
        return BASE_URL + "fine/pay";
    }
    public static String GET_PROBLEMS() {
        return BASE_URL + "problem/get";
    }
    public static String REPORT_PROBLEMS() {
        return BASE_URL + "problem/input";
    }
    public static String REGISTER_GUEST_URL() {
        return BASE_URL + "visitor/register";
    }
    public static String UNREGISTER_GUEST_URL() {
        return BASE_URL + "visitor/unregister";
    }
    public static String GET_GUEST_DETAIL_URL(){
        return BASE_URL + "visitor/detail/{cardID}";
    }
    public static String REGISTER_MEMBER_URL() {
        return BASE_URL + "user/register";
    }
    public static String GET_VEHICLE_TYPE_URL() {
        return BASE_URL + "vehicle/type";
    }
    public static String GET_MEMBER_ID_URL() {
        return BASE_URL + "user/{username}/getMemberID";
    }
    public static String ADD_VEHICLE_URL() {
        return BASE_URL + "vehicle/add";
    }
    public static String IMPORT_MEMBER_DATA_URL() {
        return BASE_URL + "member/import";
    }
    public static String EXPORT_MEMBER_DATA_URL() {
        return BASE_URL + "user/allMember";
    }

    public static void STORE_PERSISTENT() {
        String combined = Util.ACCESS_TOKEN + ":" + Util.USER_ID + ":" + Util.SESSION_ID;
        String setup = Util.PARKING_LOT_ID + ":" + Util.GATE_ID + ":" + Util.GATE_TYPE + ":" + Util.VEHICLE_TYPE_ID;
        combined = combined.replace("\"", "");
        setup = setup.replace("\"", "");
        try {
            File f = new File("t.fs");
            File s = new File("s.fs");
            f.createNewFile();
            s.createNewFile();
            byte[] data = combined.getBytes("UTF-8");
            FileOutputStream out = new FileOutputStream("t.fs");
            out.write(data);
            out.close();

            data = setup.getBytes("UTF-8");
            out = new FileOutputStream("s.fs");
            out.write(data);
            out.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void LOAD_PERSISTENT() {
        try {
            File f = new File("t.fs");
            if (!f.exists()) {
                return;
            }

            BufferedReader br = new BufferedReader(new FileReader("t.fs"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            String result = sb.toString();
            String[] results = result.split(":");
            Util.ACCESS_TOKEN = results[0];
            Util.USER_ID = Integer.parseInt(results[1]);
            if (results.length > 2) {
                Util.SESSION_ID = results[2];
            }
            br.close();

            f = new File("s.fs");
            if (!f.exists()) {
                return;
            }

            br = new BufferedReader(new FileReader("s.fs"));
            sb = new StringBuilder();
            line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
            results = result.split(":");
            Util.PARKING_LOT_ID = Integer.parseInt(results[0]);
            Util.GATE_ID = results[1];
            Util.GATE_TYPE = results[2];
            Util.VEHICLE_TYPE_ID = results[3];
            System.out.println("util.Util.LOAD_PERSISTENT() VEHICLE TYPEID: "+results[3]);
            br.close();
            
            f = new File("env.fs");
            if (!f.exists()) {
                String combined = Util.BASE_URL + ";" + Util.READ_DIRECTORY + ";" + Util.GATE_DIRECTORY;
                combined = combined.replace("\"", "");
                try {
                    File f1 = new File("env.fs");

                    f1.createNewFile();
                    byte[] data = combined.getBytes("UTF-8");
                    FileOutputStream out = new FileOutputStream("env.fs");
                    out.write(data);
                    out.close();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            br = new BufferedReader(new FileReader("env.fs"));
            sb = new StringBuilder();
            line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
            results = result.split(";");
            Util.BASE_URL = results[0];
            Util.READ_DIRECTORY = results[1];
            Util.GATE_DIRECTORY = results[2];
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void logout(JFrame frame) {
        Util.ACCESS_TOKEN = "";
        Util.USER_ID = 0;
        Util.STORE_PERSISTENT();
        
        Webcam webcam = Webcam.getDefault();
        webcam.close();

        LoginFrame loginFrame = new LoginFrame();
        loginFrame.setVisible(true);
        loginFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.dispose();
    }

    public static void logout(Frame frame) {
        Util.ACCESS_TOKEN = "";
        Util.USER_ID = 0;
        Util.STORE_PERSISTENT();
        
        Webcam webcam = Webcam.getDefault();
        webcam.close();

        LoginFrame loginFrame = new LoginFrame();
        loginFrame.setVisible(true);
        loginFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.dispose();
    }

    public static void logout(JDialog dialog) {
        Util.ACCESS_TOKEN = "";
        Util.USER_ID = 0;
        Util.STORE_PERSISTENT();
        
        Webcam webcam = Webcam.getDefault();
        webcam.close();

        LoginFrame loginFrame = new LoginFrame();
        loginFrame.setVisible(true);
        loginFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        dialog.dispose();
    }

    /**
     * Encodes the byte array into base64 string
     *
     * @param imageByteArray - byte array
     * @return String a {@link java.lang.String}
     */
    public static String encodeImage(byte[] imageByteArray) {
        String base64 = java.util.Base64.getEncoder().encodeToString(imageByteArray);
        base64 = base64.replace("\n","");
        return base64;
    }

    /**
     * Decodes the base64 string into byte array
     *
     * @param imageDataString - a {@link java.lang.String}
     * @return byte array
     */
    public static byte[] decodeImage(String imageDataString) {
        return Base64.decodeBase64(imageDataString);
    }

    public static String CountDuration(String inTime, String outTime) {

        SimpleDateFormat simpleDateFormat
                = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String duration = "";
        try {
            Date startDate = simpleDateFormat.parse(inTime);
            Date endDate = simpleDateFormat.parse(outTime);
            long different = endDate.getTime() - startDate.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            duration = "";
            if (elapsedDays == 0) {
                duration += "";
            } else {
                duration += String.valueOf(elapsedDays) + "days ";
            }

            if (elapsedHours == 0) {
                duration += "";
            } else {
                duration += String.valueOf(elapsedHours) + "hours ";
            }

            if (elapsedMinutes == 0) {
                duration += "";
            } else {
                duration += String.valueOf(elapsedMinutes) + "mins ";
            }

            if (elapsedSeconds == 0) {
                duration += "";
            } else {
                duration += String.valueOf(elapsedSeconds) + "secs ";
            }
        } catch (ParseException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }

        return duration;
    }

    public static String localizeDate(String input) {
        SimpleDateFormat simpleDateFormat
                = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String output = "";
        try {
            Date date = simpleDateFormat.parse(input);
            output = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy").format(date);
        } catch (ParseException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }
}
