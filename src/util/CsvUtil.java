/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author Ferico
 */
public class CsvUtil {
    public static ArrayList<ArrayList<String>> parse(boolean isSkipHeader, File csvFile){
        String line = "";
        String cvsSplitBy = ",";
        ArrayList<ArrayList<String>> result = new ArrayList<>();
        
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            if(isSkipHeader)
                br.readLine();
            while ((line = br.readLine()) != null) {
                ArrayList<String> row = new ArrayList<>();
                // use comma as separator
                String[] cells = line.split(cvsSplitBy);
                for(String cell : cells) {
                    row.add(cell);
                }
                result.add(row);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return result;
    }
    
    public static ArrayList<String> write(ArrayList<User>userList, String fileName)
    {
        ArrayList<String> result = new ArrayList<>();
        String header = "id,member_type_id,username,email,password,phoneNumber,point,isBanned,alamatTinggal,alamatKTP,noKTP,auditedUser,auditedActivity,created_at,updated_at,remember_token";
        result.add(header);
        
        for(User user: userList) {
            String str = user.getId()+","+user.getMember_type_id()+","+user.getUsername()
                    +","+user.getEmail()+",,"+user.getPhoneNumber()+","+user.getPoint()
                    +","+user.getIsBanned()+","+user.getAlamatTinggal()+","+user.getAlamatKTP()
                    +","+user.getNoKTP()+","+user.getAuditedUser()+","+user.getAuditedActivity()
                    +","+user.getCreated_at()+","+user.getUpdated_at()+","+user.getRemember_token();
            result.add(str);
        }
        return result;
    }
}
