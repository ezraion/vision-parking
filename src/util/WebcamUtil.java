/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.github.sarxos.webcam.Webcam;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import visionparking.LoginFrame;

/**
 *
 * @author Ferico
 */
public class WebcamUtil {
    public static void takePicture(String filename) {
        //for debugging
//        Webcam webcam = Webcam.getDefault();
//        try {
//            BufferedImage img = webcam.getImage();
//            ImageIO.write(img, "JPG", new File(filename+".jpg"));
//        } catch (IOException ex) {
//            Logger.getLogger(WebcamUtil.class.getName()).log(Level.SEVERE, null, ex);
//        }
        //for raspberry
        try {
            Process p = Runtime.getRuntime().exec("fswebcam "+filename+".jpg");
            p.waitFor();
        } catch (IOException ex) {
            Logger.getLogger(LoginFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(WebcamUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void takePicture(String filename, int cameraIndex) {
        try {
            Process p = Runtime.getRuntime().exec("fswebcam -d /dev/video"+cameraIndex+" "+filename+".jpg");
            p.waitFor();
        } catch (IOException ex) {
            Logger.getLogger(LoginFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(WebcamUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
