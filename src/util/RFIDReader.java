/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import visionparking.LoginFrame;

/**
 *
 * @author Ferico
 */
public class RFIDReader implements Runnable{

    public interface RFIDReaderListener
    {
        void onTagDetected(String tagID);
    }
    
    private RFIDReaderListener callback;
    
    public RFIDReader(RFIDReaderListener callback)
    {
        this.callback = callback;
    }
    
    @Override
    public void run() {
        while(true)
        {
            BufferedWriter out;
            try {
                //out = new BufferedWriter(new FileWriter("path/a.py"));
                //Runtime.getRuntime().exec("sudo su");
                
                
                if(Util.READ_DIRECTORY.equals(" "))
                    return;
                Process p = Runtime.getRuntime().exec("python3 "+Util.READ_DIRECTORY);
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String ret = in.readLine();
                System.out.println("value is : "+ret);
                if(ret != null)
                {
                    if(!ret.equals("E1") && !ret.equals("E2"))
                        callback.onTagDetected(ret);
                }
                
                
            } catch (IOException ex) {
                Logger.getLogger(LoginFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
