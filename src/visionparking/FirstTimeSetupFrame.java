/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visionparking;

import javax.swing.JFrame;
import util.Util;

/**
 *
 * @author Alan
 */
public class FirstTimeSetupFrame extends javax.swing.JFrame {

    /**
     * Creates new form FirstTimeSetupFrame
     */
    public FirstTimeSetupFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        topPanel = new javax.swing.JPanel();
        lblHeader = new javax.swing.JLabel();
        centerPanel = new javax.swing.JPanel();
        cntPane = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtParkingLotID = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtGateID = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cbxGateType = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtVehicleType = new javax.swing.JTextField();
        btnPanel = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        titlePanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        topPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 0, 0));

        lblHeader.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/header.jpg"))); // NOI18N
        topPanel.add(lblHeader);

        getContentPane().add(topPanel, java.awt.BorderLayout.PAGE_START);

        centerPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(40, 300, 100, 300));
        centerPanel.setLayout(new java.awt.BorderLayout());

        cntPane.setBorder(javax.swing.BorderFactory.createEmptyBorder(75, 75, 75, 75));
        cntPane.setLayout(new java.awt.GridLayout(4, 2, 10, 20));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Parking Lot ID");
        cntPane.add(jLabel1);
        cntPane.add(txtParkingLotID);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Gate ID");
        cntPane.add(jLabel2);
        cntPane.add(txtGateID);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Gate Type");
        cntPane.add(jLabel3);

        cbxGateType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "In Gate", "Out Gate", "Automatic Out Gate" }));
        cntPane.add(cbxGateType);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Vehicle Type ID");
        cntPane.add(jLabel5);
        cntPane.add(txtVehicleType);

        centerPanel.add(cntPane, java.awt.BorderLayout.CENTER);

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton1.setText("Save");
        jButton1.setPreferredSize(new java.awt.Dimension(150, 50));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        btnPanel.add(jButton1);

        centerPanel.add(btnPanel, java.awt.BorderLayout.SOUTH);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel4.setText("First Time Setup");
        titlePanel.add(jLabel4);

        centerPanel.add(titlePanel, java.awt.BorderLayout.NORTH);

        getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(txtGateID.getText().length() == 0 || txtParkingLotID.getText().length() == 0
                || txtVehicleType.getText().length() == 0)
            return;
        Util.GATE_ID = txtGateID.getText();
        Util.VEHICLE_TYPE_ID = txtVehicleType.getText();
        Util.PARKING_LOT_ID = Integer.parseInt(txtParkingLotID.getText());
        Util.GATE_TYPE = cbxGateType.getSelectedItem().toString();
        Util.STORE_PERSISTENT();
        
        LoginFrame loginFrame = new LoginFrame();
        loginFrame.setVisible(true);
        loginFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FirstTimeSetupFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FirstTimeSetupFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FirstTimeSetupFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FirstTimeSetupFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FirstTimeSetupFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel btnPanel;
    private javax.swing.JComboBox<String> cbxGateType;
    private javax.swing.JPanel centerPanel;
    private javax.swing.JPanel cntPane;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel lblHeader;
    private javax.swing.JPanel titlePanel;
    private javax.swing.JPanel topPanel;
    private javax.swing.JTextField txtGateID;
    private javax.swing.JTextField txtParkingLotID;
    private javax.swing.JTextField txtVehicleType;
    // End of variables declaration//GEN-END:variables
}
