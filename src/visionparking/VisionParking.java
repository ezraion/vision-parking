/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visionparking;

import com.google.zxing.WriterException;
import com.google.zxing.qrcode.encoder.QRCode;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintException;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.http.client.fluent.Async;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.concurrent.FutureCallback;
import ui.TicketResultFrame;
import util.PrinterUtil;
import util.QRCodeUtil;


/**
 *
 * @author Alan
 */
public class VisionParking {

    /**
     * @param args the command line arguments
     */
    

    public VisionParking() {
        //Buat contoh
        //generateQrCodeAndPrint();
        
        JFrame lgnFrame = new LoginFrame();
        lgnFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        lgnFrame.setVisible(true);

    }

    public static void main(String[] args) {
        new VisionParking();
    }
    
    void generateQrCodeAndPrint(){
        try {
            QRCodeUtil.createQRCode("123456", "qrcode.png", 10, 10);
        } catch (WriterException ex) {
            Logger.getLogger(VisionParking.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VisionParking.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            PrinterUtil.print("qrcode.png");
        } catch (PrintException ex) {
            Logger.getLogger(VisionParking.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VisionParking.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Contoh Async Call
    void  testAsync(){
        ExecutorService threadpool = Executors.newFixedThreadPool(1);
        Async async = Async.newInstance().use(threadpool);
        
        Future<Content> future = async.execute(Request.Get("https://jsonplaceholder.typicode.com/users"), new FutureCallback<Content>() {
            @Override
            public void completed(Content t) {
                //JOptionPane.showMessageDialog(null, t.asString());
            }

            @Override
            public void failed(Exception excptn) {
                //JOptionPane.showMessageDialog(null, excptn.getMessage());
            }

            @Override
            public void cancelled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
    
}
