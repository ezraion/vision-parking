/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visionparking;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.AddVehicleRequest;
import model.Problem;
import model.VehicleType;
import ui.JLoadingDialog;
import util.RFIDReader;
import util.Util;
import util.WebcamUtil;
import webservice.ParkingWS;
import webservice.WSListener;

/**
 *
 * @author Ferico
 */
public class AddVehiclePanel extends javax.swing.JPanel implements WSListener, RFIDReader.RFIDReaderListener {

    private JFrame parent;
    ParkingWS ws;
    JLoadingDialog loading;
    private String imageDataString = "";
    private ArrayList<VehicleType> vehicleTypes = new ArrayList<>();
    private String memberID = "";
    private boolean isRequesting = false;
    /**
     * Creates new form AddVehiclePanel
     */
    public AddVehiclePanel() {
        initComponents();
    }
    public AddVehiclePanel(JFrame parent) {
        initComponents();
        this.parent = parent;
        loading = new JLoadingDialog(parent, true);
        
        resetData();
        listen();
        getVehicleTypes();
    }
    
    private void getVehicleTypes() {
        ws = new ParkingWS();
        ws.getVehicleTypes(new WSListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                loading.dispose();
                JsonArray data = jsonObject.getAsJsonArray("data");
                comboVehicleType.removeAllItems();
                for(JsonElement obj : data){
                    JsonObject o = obj.getAsJsonObject();
                    VehicleType type = new VehicleType();
                    type.setId(Integer.parseInt(o.get("id").getAsString()));
                    type.setVehicleType(o.get("vehicleType").getAsString());
                    vehicleTypes.add(type);
                    comboVehicleType.addItem(type.getVehicleType());
                }
            }

            @Override
            public void onError(String error, JsonObject jsonObject) {
                loading.dispose();
                JOptionPane.showMessageDialog(AddVehiclePanel.this, error, "Error", JOptionPane.ERROR_MESSAGE);
            }

            @Override
            public void shouldLogOut() {
                loading.dispose();
                Util.logout(parent);
            }
        });
        loading.setVisible(true);
    }
    
    private void resetData() {
        txtAddress.setText("");
        txtBrand.setText("");
        txtCardID.setText("");
        txtColor.setText("");
        txtName.setText("");
        txtNoSTNK.setText("");
        txtPlateNumber.setText("");
        txtType.setText("");
        txtUsername.setText("");
        
        try {
            BufferedImage noImg = ImageIO.read(ClassLoader.getSystemResource("img/No-img.png"));
            Image scaledImg = noImg.getScaledInstance(480, 200, Image.SCALE_SMOOTH);
            imgSTNK.setIcon(new ImageIcon(scaledImg));
        } catch (IOException ex) {
            Logger.getLogger(NonMemberPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void listen()
    {
        RFIDReader reader = new RFIDReader(this);
        Thread t = new Thread(reader);
        t.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        leftPanel = new javax.swing.JPanel();
        masukPanel = new javax.swing.JPanel();
        lblPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        panelKTP = new javax.swing.JPanel();
        imgSTNK = new javax.swing.JLabel();
        rightPanel = new javax.swing.JPanel();
        bottomPanel = new javax.swing.JPanel();
        btnCapture = new javax.swing.JButton();
        btnAddVehicle = new javax.swing.JButton();
        formPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCardID = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        txtName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        comboVehicleType = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtPlateNumber = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtNoSTNK = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtBrand = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtType = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtColor = new javax.swing.JTextField();

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 50, 50, 50));
        jPanel1.setLayout(new java.awt.GridLayout(1, 2));

        leftPanel.setLayout(new java.awt.GridLayout(2, 0));

        masukPanel.setLayout(new java.awt.BorderLayout());

        lblPanel.setPreferredSize(new java.awt.Dimension(263, 50));
        lblPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Foto STNK");
        lblPanel.add(jLabel1);

        masukPanel.add(lblPanel, java.awt.BorderLayout.PAGE_START);

        panelKTP.add(imgSTNK);

        masukPanel.add(panelKTP, java.awt.BorderLayout.CENTER);

        leftPanel.add(masukPanel);

        jPanel1.add(leftPanel);

        rightPanel.setLayout(new java.awt.BorderLayout());

        bottomPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 5));

        btnCapture.setBackground(new java.awt.Color(254, 0, 0));
        btnCapture.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCapture.setText("Capture Photo");
        btnCapture.setPreferredSize(new java.awt.Dimension(150, 80));
        btnCapture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCaptureActionPerformed(evt);
            }
        });
        bottomPanel.add(btnCapture);

        btnAddVehicle.setBackground(new java.awt.Color(0, 119, 29));
        btnAddVehicle.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAddVehicle.setText("Add Vehicle");
        btnAddVehicle.setPreferredSize(new java.awt.Dimension(150, 80));
        btnAddVehicle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddVehicleActionPerformed(evt);
            }
        });
        bottomPanel.add(btnAddVehicle);

        rightPanel.add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        formPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 10, 1, 1));
        formPanel.setLayout(new java.awt.GridLayout(16, 2));

        jLabel3.setText("Username");
        formPanel.add(jLabel3);
        formPanel.add(txtUsername);

        jLabel4.setText("Card ID");
        formPanel.add(jLabel4);
        formPanel.add(txtCardID);

        jPanel2.setLayout(new java.awt.GridLayout(1, 2));

        jLabel9.setText("Vehicle Name");
        jPanel2.add(jLabel9);

        formPanel.add(jPanel2);

        jPanel3.setLayout(new java.awt.GridLayout(1, 3, 3, 0));
        jPanel3.add(txtName);

        formPanel.add(jPanel3);

        jLabel5.setText("Vehicle Type");
        formPanel.add(jLabel5);

        comboVehicleType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        formPanel.add(comboVehicleType);

        jLabel6.setText("Plate Number");
        formPanel.add(jLabel6);
        formPanel.add(txtPlateNumber);

        jLabel7.setText("Address");
        formPanel.add(jLabel7);
        formPanel.add(txtAddress);

        jLabel8.setText("No STNK");
        formPanel.add(jLabel8);
        formPanel.add(txtNoSTNK);

        jLabel10.setText("Brand");
        formPanel.add(jLabel10);
        formPanel.add(txtBrand);

        jLabel11.setText("Type");
        formPanel.add(jLabel11);
        formPanel.add(txtType);

        jLabel12.setText("Color");
        formPanel.add(jLabel12);
        formPanel.add(txtColor);

        rightPanel.add(formPanel, java.awt.BorderLayout.CENTER);

        jPanel1.add(rightPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 528, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCaptureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCaptureActionPerformed
        WebcamUtil.takePicture("stnk");
        File file = new File("stnk.jpg");
        FileInputStream imageInFile;
        try {
            imageInFile = new FileInputStream(file);
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            
            BufferedImage noImg = ImageIO.read(file);
            Image scaledImg = noImg.getScaledInstance(480, 200, Image.SCALE_SMOOTH);
            imgSTNK.setIcon(new ImageIcon(scaledImg));

            // Converting Image byte array into Base64 String
            imageDataString = Util.encodeImage(imageData);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NonMemberPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NonMemberPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnCaptureActionPerformed

    private void btnAddVehicleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddVehicleActionPerformed
        if(imageDataString.equals("")) {
            JOptionPane.showMessageDialog(null, "KTP Photo must be provided!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtCardID.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtAddress.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtUsername.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtType.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtPlateNumber.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtNoSTNK.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtName.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtColor.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(txtBrand.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "All field must be filled!", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if(isRequesting)
            return;
        
        ws = new ParkingWS();
        final int vehicleTypeID = vehicleTypes.get(comboVehicleType.getSelectedIndex()).getId();
        ws.getMemberID(txtUsername.getText(), new WSListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                loading.dispose();
                JsonObject data = jsonObject.getAsJsonObject("data");
                memberID = data.get("id").getAsString();
                ParkingWS ws1 = new ParkingWS();
                
                if(isRequesting)
                    return;
                isRequesting = true;
                AddVehicleRequest request = new AddVehicleRequest(vehicleTypeID, memberID, 
                        txtCardID.getText(), txtName.getText(), txtPlateNumber.getText(), 
                        txtAddress.getText(), txtNoSTNK.getText(), txtBrand.getText(), 
                        txtColor.getText(), txtType.getText(), imageDataString);
                ws1.addVehicle(request, AddVehiclePanel.this);
                loading.setVisible(true);
            }

            @Override
            public void onError(String error, JsonObject jsonObject) {
                loading.dispose();
                JOptionPane.showMessageDialog(AddVehiclePanel.this, error, "Error", JOptionPane.ERROR_MESSAGE);
            }

            @Override
            public void shouldLogOut() {
                loading.dispose();
                Util.logout(parent);
            }
        });
        loading.setVisible(true);
    }//GEN-LAST:event_btnAddVehicleActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JButton btnAddVehicle;
    private javax.swing.JButton btnCapture;
    private javax.swing.JComboBox<String> comboVehicleType;
    private javax.swing.JPanel formPanel;
    private javax.swing.JLabel imgSTNK;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel lblPanel;
    private javax.swing.JPanel leftPanel;
    private javax.swing.JPanel masukPanel;
    private javax.swing.JPanel panelKTP;
    private javax.swing.JPanel rightPanel;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtBrand;
    private javax.swing.JTextField txtCardID;
    private javax.swing.JTextField txtColor;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtNoSTNK;
    private javax.swing.JTextField txtPlateNumber;
    private javax.swing.JTextField txtType;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onSuccess(JsonObject jsonObject) {
        loading.dispose();
        JOptionPane.showMessageDialog(this, "Vehicle Saved!", "Success!", JOptionPane.INFORMATION_MESSAGE);
        resetData();
        isRequesting = false;
    }

    @Override
    public void onError(String error, JsonObject jsonObject) {
        loading.dispose();
        JOptionPane.showMessageDialog(this, error, "Error", JOptionPane.ERROR_MESSAGE);
        isRequesting = false;
    }

    @Override
    public void shouldLogOut() {
        loading.dispose();
        Util.logout(parent);
        isRequesting = false;
    }

    @Override
    public void onTagDetected(String tagID) {
        txtCardID.setText(tagID);
        isRequesting = false;
    }
}
